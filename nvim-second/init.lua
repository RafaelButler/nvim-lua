require('plugins')
require('keymaps')
require('options')
require('lsp')
require('treesiter')
require('conf-cmp')
require('autopairs')
require('nvimtree')
require('telescope')

require("bufferline").setup{}
require('Comment').setup()
